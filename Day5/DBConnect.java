package com.sapient.week1.Day5;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {
	public static final String username = "system";
	public static final String password = "password";
	private static Connection conn = null;
	public static Connection getConnection() throws Exception{
		if(conn == null) {
			conn = DriverManager.getConnection("jdbc:oracle:thin:"+username+"/"+password+"@localhost:1521:xe");
		}
		return conn;
	}
}
