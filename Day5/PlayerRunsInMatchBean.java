package com.sapient.week1.Day5;

public class PlayerRunsInMatchBean {
	private int matchId;
	private int runs;
	private int playerId;
	public PlayerRunsInMatchBean() {
		this.matchId = Integer.MIN_VALUE;
		this.runs = Integer.MIN_VALUE;
		this.playerId = Integer.MIN_VALUE;
	}
	public PlayerRunsInMatchBean(int matchId, int runs, int playerId) {
		this.matchId = matchId;
		this.runs = runs;
		this.playerId = playerId;
	}
	public int getMatchId() {
		return matchId;
	}
	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}
	public int getRuns() {
		return runs;
	}
	public void setRuns(int runs) {
		this.runs = runs;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	@Override
	public String toString() {
		return "PlayerRunsInMatchBean [matchId=" + matchId + ", runs=" + runs + ", playerId=" + playerId + "]";
	}
	
}
