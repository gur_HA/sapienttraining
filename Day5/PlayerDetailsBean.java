package com.sapient.week1.Day5;

public class PlayerDetailsBean {
	private int playerId;
	private String firstName;
	private String lastName;
	private int jerseyNo;
	public PlayerDetailsBean() {
		this.playerId = Integer.MIN_VALUE;
		this.playerId = Integer.MIN_VALUE;
		this.playerId = Integer.MIN_VALUE;
		this.jerseyNo = Integer.MIN_VALUE;
	}
	public PlayerDetailsBean(int playerID, String firstName, String lastName, int jerseyNo) {
		
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerID) {
		this.playerId = playerID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getJerseyNo() {
		return jerseyNo;
	}
	public void setJerseyNo(int jerseyNo) {
		this.jerseyNo = jerseyNo;
	}
	@Override
	public String toString() {
		return "PlayerDetailsBean [playerId=" + playerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", jerseyNo=" + jerseyNo + "]";
	}
	
}
