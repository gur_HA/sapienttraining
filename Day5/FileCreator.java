package com.sapient.week1.Day5;

// Complete this code.

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

public class FileCreator {
	Random rand;
	BufferedWriter writer; 
	class Data{
		private int r;
		private int g;
		private int b;
		private int a;
		public Data(int r, int g, int b, int a) {
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
		public int getR() {
			return r;
		}
		public void setR(int r) {
			this.r = r;
		}
		public int getG() {
			return g;
		}
		public void setG(int g) {
			this.g = g;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
		
	}
	public void createFile() {
		try {
			writer = new BufferedWriter(new FileWriter(FILE_PATH));
			rand = new Random();
			
			for (int i = 0; i < NUM_OBJECTS; i++) {
				int r = rand.nextInt(MAX_DATA_VALUE+1);
				int g = rand.nextInt(MAX_DATA_VALUE+1);
				int b = rand.nextInt(MAX_DATA_VALUE+1);
				int a = MAX_DATA_VALUE;
				
				String Data = String.format(DATA_FORMAT, r);
				Data += JOIN_TOKEN + String.format(DATA_FORMAT, g); 
				Data += JOIN_TOKEN + String.format(DATA_FORMAT, b); 
				Data += JOIN_TOKEN + String.format(DATA_FORMAT, a);
				
				System.out.println("Iteration "+i+": "+Data);
				writer.write(Data);
			}
		}catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	public static void main(String[] args) {
		FileCreator fc = new FileCreator();
		fc.createFile();
	}
	public static final String DATA_FORMAT = "%03d";
	public static final int MAX_DATA_VALUE = 255;
	public static final int MIN_DATA_VALUE = 0;
	public static final int NUM_OBJECTS = 286331153;
	public static final String FILE_PATH = "C:\\Users\\gursingh28\\Desktop\\Database\\gigaFile.csv";
	public static final String JOIN_TOKEN = ",";
}
