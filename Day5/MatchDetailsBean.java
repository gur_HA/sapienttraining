package com.sapient.week1.Day5;

public class MatchDetailsBean {
	private int matchId;
	private String matchName;
	public MatchDetailsBean(int matchId, String matchName) {
		super();
		this.matchId = matchId;
		this.matchName = matchName;
	}
	public MatchDetailsBean() {
		super();
	}
	@Override
	public String toString() {
		return "MatchDetailsBean [matchId=" + matchId + ", matchName=" + matchName + "]";
	}
	public int getMatchId() {
		return matchId;
	}
	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}
	public String getMatchName() {
		return matchName;
	}
	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}
}
