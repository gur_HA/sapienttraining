package com.sapient.week1.Day5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	public static final String query1 = "SELECT * FROM PlayerDetails";
	public static final String query2 = "INSERT INTO PlayerDetails (PlayerId, FirstName, LastName, JerseyNo) VALUES (?,?,?,?)";
	public static final String query3 = "DELETE FROM PlayerDetails WHERE PlayerId = ?";
	public static final String query4 = "UPDATE PlayerDetails SET FirstName = ?, LastName = ?, JerseyNo = ? WHERE PlayerId = ?";
	public static final String commitCommand = "COMMIT";
	public static final int PLAYER_ID = 1;
	public static final int FIRST_NAME = 2;
	public static final int LAST_NAME = 3;
	public static final int JERSEY_NO = 4;
	
	public List<PlayerDetailsBean> getPlayers() throws Exception{
		Connection conn = DBConnect.getConnection();
		PreparedStatement ps = conn.prepareStatement(query1);
		ResultSet rs = ps.executeQuery();
		List<PlayerDetailsBean> players = convertResultSetToList(rs);
		System.out.println(players.size());
		conn=null;
		return players;
	}	
	public void insertPlayer(PlayerDetailsBean pdb) throws Exception {
		Connection conn = DBConnect.getConnection();
		PreparedStatement ps = conn.prepareStatement(query2);
		
		ps.setInt(PLAYER_ID, pdb.getPlayerId());
		ps.setString(FIRST_NAME, pdb.getFirstName());
		ps.setString(LAST_NAME, pdb.getLastName());
		ps.setInt(JERSEY_NO, pdb.getJerseyNo());
		
		ps.executeQuery();
		PreparedStatement commit = conn.prepareStatement(commitCommand);
		commit.executeQuery();
		
		conn=null;
	}
	public void deletePlayer(PlayerDetailsBean pdb) throws Exception {
		Connection conn = DBConnect.getConnection();
		PreparedStatement ps = conn.prepareStatement(query3);
		
		ps.setInt(PLAYER_ID, pdb.getPlayerId());
		ps.executeQuery();
		
		PreparedStatement commit = conn.prepareStatement(commitCommand);
		commit.executeQuery();
		conn=null;
	}
	public void updatePlayer(PlayerDetailsBean pdb) throws Exception {
		Connection conn = DBConnect.getConnection();
		PreparedStatement ps = conn.prepareStatement(query4);
		
		ps.setString(FIRST_NAME, pdb.getFirstName());
		ps.setString(LAST_NAME, pdb.getLastName());
		ps.setInt(JERSEY_NO, pdb.getJerseyNo());
		ps.setInt(PLAYER_ID, pdb.getPlayerId());
		ps.executeQuery();
		
		PreparedStatement commit = conn.prepareStatement(commitCommand);
		commit.executeQuery();
		conn=null;
	}
	
	private List<PlayerDetailsBean> convertResultSetToList(ResultSet rs) throws SQLException {
		List<PlayerDetailsBean> players = new ArrayList<>();	
		while(rs.next()) {
			players.add(new PlayerDetailsBean(
					rs.getInt(PLAYER_ID)
					,rs.getString(FIRST_NAME)
					,rs.getString(LAST_NAME)
					,rs.getInt(JERSEY_NO)
					));
		}
		return players;
	}
}
