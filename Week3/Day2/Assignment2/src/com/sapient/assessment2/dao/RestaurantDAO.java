package com.sapient.assessment2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.net.URI;

import javax.json.JsonObject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sapient.assessment2.beans.RestaurantBean;

@Path("/restaurants")
public class RestaurantDAO {
	public static List<RestaurantBean> getRestaurants(String category) {
		List<RestaurantBean> result = getQueryResult(category);
		return result;
	}
	
	private static List<RestaurantBean> getQueryResult(String entityId) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		URI uri = getQueryURI(entityId);
		WebTarget target = client.target(uri);
		String jsonResponse = target.request()
				.header("user-key", "e3d298a4f18a35080b395607d3cad746")
				.accept(MediaType.APPLICATION_JSON)
				.get(String.class);
		return getRestaurantsList(jsonResponse);
	}

	private static List<RestaurantBean> getRestaurantsList(String jsonResponse) {
		List<RestaurantBean> restaurants = new ArrayList<>();
		try {
			JSONObject json = new JSONObject(jsonResponse);
			JSONArray restaurantsArray = json.getJSONArray("restaurants");
			for (int i = 0; i < restaurantsArray.length(); i++) {
				JSONObject restaurant = (JSONObject)restaurantsArray.get(i);
				
				restaurant = (JSONObject)restaurant.get("restaurant");
				
				String name = restaurant.getString("name");
				String url = restaurant.getString("url");
				
				JSONObject user_rating = new JSONObject(restaurant.getString("user_rating"));
				String rating = user_rating.getString("aggregate_rating");
				
				restaurants.add(new RestaurantBean(name, url, rating));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return restaurants;
	}
	
	private static URI getQueryURI(String entityId) {
		String url = baseUrl + "?entity_id=" + entityId + "&entity_type=city";
		return UriBuilder.fromUri(baseUrl).build();
	}
	public static void main(String args[]) {
		try {
			List<RestaurantBean> rests = getRestaurants("1");
			for (RestaurantBean restaurantBean : rests) {
				System.out.println(restaurantBean);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	private static final String baseUrl = "https://developers.zomato.com/api/v2.1/search";
}
