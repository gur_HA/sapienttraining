package com.sapient.week2.Day3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoginDAO {
	public static final String query1 = "SELECT username, password, role FROM users WHERE username=?";
	public static final String commitCommand = "COMMIT";
	public static final int USERNAME = 1;
	public static final int PASSWORD = 2;
	public static final int ROLE = 3;
	private static LoginDAO sdao;
	private LoginDAO() {
		super();
	}
	public static LoginDAO getInstance() {
		if(sdao==null) {
			sdao = new LoginDAO();
		}
		return sdao;
	}
	public UserBean getUserDetails(String username) throws SQLException, RecordNotFoundException, ClassNotFoundException{
		Connection conn = DBConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(query1);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		List<UserBean> players = convertResultSetToList(rs);
		if(players.size()>0) {
			return players.get(0);
		}
		throw new RecordNotFoundException();
	}
	private List<UserBean> convertResultSetToList(ResultSet rs) throws SQLException {
		List<UserBean> players = new ArrayList<>();	
		while(rs.next()) {
			players.add(new UserBean(
					 rs.getString(USERNAME)
					,rs.getString(PASSWORD)
					,rs.getString(ROLE)
					));
		}
		return players;
	}
}
