package com.sapient.week2.Day3;

public class StudentBean {
	
	private Integer Marks;
	private String firstName;
	private String lastName;
	private String registartionId;
	public StudentBean() {
		// TODO Auto-generated constructor stub
		super();
	}
	public StudentBean(Integer marks, String firstName, String lastName, String registartionId) {
		super();
		Marks = marks;
		this.firstName = firstName;
		this.lastName = lastName;
		this.registartionId = registartionId;
	}
	public Integer getMarks() {
		return Marks;
	}
	public void setMarks(Integer marks) {
		Marks = marks;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRegistartionId() {
		return registartionId;
	}
	public void setRegistartionId(String registartionId) {
		this.registartionId = registartionId;
	}
}
