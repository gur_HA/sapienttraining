package com.example.demo;

public class StudentBean {
	private String name;
	private String age;
	private String city;
	public StudentBean() {
		super();
	}
	public StudentBean(String name, String age, String city) {
		super();
		this.name = name;
		this.age = age;
		this.city = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "StudentBean [name=" + name + ", age=" + age + ", city=" + city + "]";
	}
	
}
