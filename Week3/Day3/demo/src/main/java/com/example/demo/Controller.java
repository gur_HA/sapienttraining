package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@RequestMapping("/hello")
	public String display() {
		return "Welcome to Microservices! :)";
	}
	@RequestMapping("/getStudent")
	public StudentBean display1() {
		return new StudentBean("Gurpal","21","New Delhi");
	}
}
