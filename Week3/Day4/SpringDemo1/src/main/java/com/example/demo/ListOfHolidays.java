package com.example.demo;

import java.util.List;

public class ListOfHolidays {
	List<Holiday> holidays;

	public ListOfHolidays() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ListOfHolidays(List<Holiday> holidays) {
		super();
		this.holidays = holidays;
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	@Override
	public String toString() {
		return "ListOfHolidays [holidays=" + holidays + "]";
	}
	
}
