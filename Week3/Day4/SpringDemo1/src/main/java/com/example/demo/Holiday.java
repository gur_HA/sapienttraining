package com.example.demo;

public class Holiday {
	String date;
	@Override
	public String toString() {
		return "Holiday [date=" + date + ", hname=" + hname + "]";
	}
	String hname;
	public Holiday() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Holiday(String date, String hname) {
		super();
		this.date = date;
		this.hname = hname;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHname() {
		return hname;
	}
	public void setHname(String hname) {
		this.hname = hname;
	}
}
