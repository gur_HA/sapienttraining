package com.example.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {
	public static void main(String[] args) {
		ApplicationContext cont = new ClassPathXmlApplicationContext("bean.xml");
		System.out.println((ListOfHolidays)cont.getBean("holidayList"));
		System.out.println((Employee)cont.getBean("emp1"));
		((ClassPathXmlApplicationContext)cont).close();
	}
}
