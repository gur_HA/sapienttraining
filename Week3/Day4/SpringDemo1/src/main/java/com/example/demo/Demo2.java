package com.example.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo2 {
	public static void main(String[] args) {
		ApplicationContext cont = new AnnotationConfigApplicationContext(JavaContainer.class);
		Hello ob = cont.getBean(Hello.class);
		ob.display();
		Employee emp = cont.getBean(Employee.class);
		System.out.println(emp);
		System.out.println(cont.getBean(ListOfHolidays.class));
		((AnnotationConfigApplicationContext)cont).close();
	}
}
