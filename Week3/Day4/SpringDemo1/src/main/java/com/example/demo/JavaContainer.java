package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaContainer {
	@Bean
	public Hello get1() {
		return new Hello();
	}
	@Bean
	public Employee get2() {
		return new Employee(21,"Gurpal");
	}
	@Bean
	public ListOfHolidays get3() {
		List<Holiday> holidays= new ArrayList<Holiday>();
		holidays.add(new Holiday("Diwali","08/11/2019"));
		return new ListOfHolidays(holidays);
	}
}
