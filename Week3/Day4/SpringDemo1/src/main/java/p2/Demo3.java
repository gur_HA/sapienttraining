package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {
	public static void main(String[] args) {
		ApplicationContext cont = new ClassPathXmlApplicationContext("bean.xml");
		try {
			Arithmetic ob = (Arithmetic)cont.getBean("arithmetic");
			double num1=10.0;
			double num2=20.0;
			System.out.println(ob.subtract(num1, num2));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} finally {
			((ClassPathXmlApplicationContext)cont).close();
		}
	}
}
