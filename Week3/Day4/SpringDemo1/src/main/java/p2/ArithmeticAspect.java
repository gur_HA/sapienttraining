package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {
	// Parameters(in Order) - Return Type, Package name.Classname (Parameters...)
	
	// CrossCut
	@Before("execution( * *.*(double,double))")
	public void check1(JoinPoint jPoint) {
		for(Object ob : jPoint.getArgs()) {
			double num = (Double) ob;
			if(num<0) {
				throw new IllegalArgumentException("Number cannot be negative.");
			}
		}
	}
	
	@AfterReturning(pointcut="execution( * *.*(double,double))", returning = "temp")
	public void check2(Object reObject) {
		double returnValue = (Double) reObject;
		if (returnValue < 0) {
			throw new IllegalStateException("Result shouldn't be negative. Abort.");
		}
	}
}
