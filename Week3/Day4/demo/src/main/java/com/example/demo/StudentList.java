package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class StudentList {
	private List<StudentBean> listStudent;
	public StudentList() {
		super();
		listStudent = new ArrayList<>();
		listStudent.add(new StudentBean("Gurpal","21","Delhi"));
		listStudent.add(new StudentBean("Gurpal","21","Delhi"));
		listStudent.add(new StudentBean("Gurpal","21","Delhi"));
		listStudent.add(new StudentBean("Gurpal","21","Delhi"));
	}
	public StudentList(List<StudentBean> listStudent) {
		super();
		this.listStudent = listStudent;
	}
	
	public List<StudentBean> getListStudent() {
		return listStudent;
	}
	public void setListStudent(List<StudentBean> listStudent) {
		this.listStudent = listStudent;
	}
	@Override
	public String toString() {
		return "StudentList [listStudent=" + listStudent + "]";
	}
	
}
