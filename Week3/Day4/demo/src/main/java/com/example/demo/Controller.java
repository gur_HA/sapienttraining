package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	private static final String HELLO = "/hello";
	@Autowired
	StudentDAO sdao;
	@RequestMapping(value = HELLO, method=RequestMethod.GET)
	public String display() {
		return "Welcome to Microservices! :)";
	}
	@RequestMapping(value = "/getStudent", method=RequestMethod.GET)
	public StudentBean display1() {
		return new StudentBean("Gurpal","21","New Delhi");
	}
	@RequestMapping(value = "/getStudents", method=RequestMethod.GET)
	public List<StudentBean> display2(){
		return sdao.getStudents();
	}
	@RequestMapping(value = "/details/{name}", method=RequestMethod.GET)
	public List<StudentBean> getName(@PathVariable("name") String name){
		return sdao.getDetails(name);
	}
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public String insert(@RequestBody StudentBean sb) {
		return sdao.insertStudent(sb);
	}
}
