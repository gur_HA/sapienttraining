package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	@Autowired
	private StudentList list;
	@Autowired
	private StudentList list2;
	public List<StudentBean> getStudents(){
		return list.getListStudent();
	}
	public List<StudentBean> getStudents2(){
		return list2.getListStudent();
	}
	public List<StudentBean> getDetails(String name){
		return list.getListStudent()
				.stream()
				.filter((sb)->(sb.getName().equals(name)))
				.collect(Collectors.toList());
	}
	public String insertStudent(StudentBean sb) {
		list.getListStudent().add(sb);
		return "Done";
	}
}
