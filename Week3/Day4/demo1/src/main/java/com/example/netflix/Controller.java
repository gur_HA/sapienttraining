package com.example.netflix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired	StudentDetailsDAO sddao;
	@Autowired	StudentMarksDAO smdao;
	@PostMapping("/addStudent")
	public String storeStudentDetails(@RequestBody StudentDetails ob) {
		sddao.save(ob);
		return "";
	}
	@PostMapping("/addMarks")
	public String storeStudentMarks(@RequestBody StudentMarks ob) {
		smdao.save(ob);
		return "";
	}
	@GetMapping("/displayStudent")
	public StudentDetails getStudentDetails() {
		sddao.
		return new StudentDetails();
	}
}
