package com.example.netflix;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StudentDetails {
	@Id 	private String id;
	@Column	private String fname;
	@Column	private String lname;
	@Column	private String dob;
	@Column	private String citycode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public StudentDetails(String id, String fname, String lname, String dob, String citycode) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.citycode = citycode;
	}
	public StudentDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
}
