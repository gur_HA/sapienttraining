package com.example.netflix;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CityDetails {
	@Id 	private String citycode;
	@Column private String cityname;
	public CityDetails(String citycode, String cityname) {
		super();
		this.citycode = citycode;
		this.cityname = cityname;
	}
	public CityDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
}
