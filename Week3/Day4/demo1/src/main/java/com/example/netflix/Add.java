package com.example.netflix;

import java.io.Serializable;

public class Add implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int num1;
	int num2;
	int num3;
	public void setData(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	public void calculate() {
		this.num3 = this.num1 + this.num2;
	}
}
