package com.example.netflix;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.log4j.Logger;

public class SerializationDemo {
	public static void main(String[] args) {
		Logger log = Logger.getLogger(SerializationDemo.class);
		Add add = new Add();
		add.setData(100, 200);
		log.error("Error", new Exception());
		ObjectOutputStream oos=null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("file.dat"));
			oos.writeObject(add);
		} catch (FileNotFoundException e) {
			log.error("Error", e);
		} catch (IOException e) {
		} finally {
			if(oos!=null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
		}
		System.out.println("written");
	}
}
