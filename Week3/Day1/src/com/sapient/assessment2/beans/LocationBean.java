package com.sapient.assessment2.beans;

public class LocationBean {
	private String locationName;
	private String latitude;
	private String longitude;
	public LocationBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LocationBean(String locationName, String latitude, String longitude) {
		super();
		this.locationName = locationName;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	@Override
	public String toString() {
		return "LocationBean [locationName=" + locationName + ", latitude=" + latitude + ", longitude=" + longitude
				+ "]";
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
