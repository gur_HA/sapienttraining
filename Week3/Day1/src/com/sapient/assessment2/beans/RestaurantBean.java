package com.sapient.assessment2.beans;

public class RestaurantBean {
	private String name;
	private String url;
	private String rating;
	public RestaurantBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RestaurantBean(String name, String url, String rating) {
		super();
		this.name = name;
		this.url = url;
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "RestaurantBean [name=" + name + ", url=" + url + ", rating=" + rating + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	
}
