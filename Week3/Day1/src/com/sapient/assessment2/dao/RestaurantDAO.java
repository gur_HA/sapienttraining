package com.sapient.assessment2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.*;
import java.net.URI;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.assessment2.beans.RestaurantBean;

@Path("/restaurants")
public class RestaurantDAO {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getRestaurants(@QueryParam("query") String query) {
		
		String restaurantsJson = "";
		return restaurantsJson; 
	}
	
	private String getQueryResult(String query) {
		ClientConfig config = new ClientConfig();
		Client client = config.getClient();
		String queryResult = "";
		return queryResult;
	}
	
	private URI getQueryURI(String uriString, String... query) {
		uriString += "?query="+query;
		return UriBuilder.fromUri(uriString).build();
	}
	private static final String baseUrl = "https://developers.zomato.com/api/v2.1/locations";
}
