package com.sapient.week1.Day4;

class Game{
	public static final int SLEEP_TIME = 100;
	int counter = 0;
	
	public static final String G1_RESPONSE = "g1 responds.";
	public static final String G2_RESPONSE = "g2 responds.";
	public static final String G3_RESPONSE = "g3 responds.";
	public synchronized void g1() {
		counter++;
		try {
			notifyAll();
			System.out.println(counter+" "+G1_RESPONSE);
			wait();
		}catch(Exception e) {
		}
	}
	public synchronized void g2() {
		counter++;
		try {
			notifyAll();
			System.out.println(counter+" "+G2_RESPONSE);
			wait();
		}catch(Exception e) {
		}
	}
	public synchronized void g3() {
		counter++;
		try {
			notifyAll();
			System.out.println(counter+" "+G3_RESPONSE);
			wait();
		}catch(Exception e) {
		}
	}
}

class Player1 extends Thread {	
	Game game;
	Player1(Game game){
		this.game = game;
	}
	public void run() {
		for(int i=0; i<10;i++) {
			game.g1();
			try {
				Thread.sleep(Game.SLEEP_TIME);
			}catch (InterruptedException e) {
			}
		}
		System.out.println("Player1 Ends.");
	}
}

class Player2 extends Thread {	
	Game game;
	Player2(Game game){
		this.game = game;
	}
	public void run() {
		for(int i=0; i<10;i++) {			
			game.g2();
			try {
				Thread.sleep(Game.SLEEP_TIME);
			}catch (InterruptedException e) {
			}
		}
		System.out.println("Player2 Ends.");
	}
}
class Player3 extends Thread {	
	Game game;
	Player3(Game game){
		this.game = game;
	}
	public void run() {
		for(int i=0; i<10;i++) {
			game.g3();
			try {				
				Thread.sleep(Game.SLEEP_TIME);
			}catch (InterruptedException e) {
			}
		}
		System.out.println("Player3 Ends.");
	}
}

public class Poker {
	public static void main(String[] args) {
		Game game = new Game();
		Player1 player1 = new Player1(game);
		Player2 player2 = new Player2(game);
//		Player3 player3 = new Player3(game);
		player1.start();
		player2.start();
//		player3.start();
	}
}
