package com.sapient.week1.Day4;

import java.util.*;
import java.io.*;
public class SqlDatabase {
	private BufferedReader reader;
	
	public void executeQuery(String query) throws Exception{
		ParsedDetails queryData = parseQuery(query);
		if (queryData == null) {
			throw new Exception("Incorrect Syntax");
		}
		
		String filePath = DATABASE_LOCATION+JOIN_TOKEN+queryData.getTableName()+FILE_EXTENSION;
		reader = new BufferedReader(new FileReader(filePath));
		
		try {
			boolean headerRead = false;
			String line;
			Map<String, Integer> headerMapping = new LinkedHashMap<>();
			List<String> columnsRequested = queryData.getColumnsRequested();
			Set<String> headerSet;
			List<String> finalHeaderList = new ArrayList<>();
			while((line = reader.readLine())!=null) {
				String[] data = line.split(ROW_REGEX);
				
				if(!headerRead) {
					for (int i = 0; i < data.length; i++) {
						headerMapping.put(data[i], i);
					}
					headerSet = headerMapping.keySet();
					for (String column : columnsRequested) {
						if(column.equalsIgnoreCase(ALL_TOKEN)) {
							for(String header: headerSet) {
								finalHeaderList.add(header);
							}
							continue;
						}
						finalHeaderList.add(column);
					}
					headerRead = true;
					continue;
				}
				
				String requestedDataLine = EMPTY_STRING;
				for (String column: finalHeaderList) {
					requestedDataLine += data[headerMapping.get(column)] + RESULT_JOIN_STRING;
				}
				requestedDataLine=requestedDataLine.substring(STARTING_INDEX, requestedDataLine.length()-1);
				
				System.out.println(requestedDataLine);
			}
		}catch(FileNotFoundException fnfe) {
			System.out.println("File not found at the given path: "+filePath);
			fnfe.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}finally {
			reader.close();
		}
	}
	
	private ParsedDetails parseQuery(String query) {
		//Split the string with the given regex.
		String[] queryTokens = query.split(SPLIT_REGEX);
		
		// if no tokens in the query and cleanup of data.
		if(queryTokens.length<=0) {
			queryTokens = null;
			System.gc();
			return null;
		}
		
		// if starting token is not SELECT and cleanup of data.
		if(!QUERY_START_TOKEN.equalsIgnoreCase(queryTokens[0])) {
			queryTokens = null;
			System.gc();
			return null;
		}
		
		
		List<String> columnsRequested = new ArrayList<>();
		int i;
		
		// keep reading columns till 'FROM' is encountered.
		for (i = 1; i < queryTokens.length; i++) {
			if(!queryTokens[i].equalsIgnoreCase(QUERY_FROM_TOKEN)) {
				columnsRequested.add(queryTokens[i]);
			}else {
				break;
			}
		}
		
		// if the loop ended by reading all the tokens without reaching at 'FROM' or
		// 'FROM' has been read but tokens are exhausted.
		// Clean up of the data.
		if(i==queryTokens.length || i+1 == queryTokens.length) {
			queryTokens = null;
			columnsRequested = null;
			System.gc();
			return null;
		}
		
		// only the first token after 'FROM' is accepted.
		i += 1;
		String tableName = queryTokens[i];
		
		// Check if there are any errors and report error, if yes.
		if(i!=queryTokens.length-1) {
			queryTokens = null;
			columnsRequested = null;
			tableName = null;
			System.gc();
			return null;
		}
		
		ParsedDetails parsedDetails = new ParsedDetails(columnsRequested, tableName);
		return parsedDetails;
	}
	
	private class ParsedDetails{
		private List<String> columnsRequested;
		private String tableName;
		ParsedDetails(List<String> columnsRequested, String tableName){
			this.columnsRequested=columnsRequested;
			this.tableName = tableName;
		}
		public List<String> getColumnsRequested() {
			return columnsRequested;
		}
		public String getTableName() {
			return tableName;
		}
		public String toString() {
			return columnsRequested + " - " + tableName; 
		}
	}
	
	public static final String ALL_TOKEN = "*";
	public static final int STARTING_INDEX = 0;
	public static final String RESULT_JOIN_STRING = ",";
	public static final String EMPTY_STRING = "";
	public static final String JOIN_TOKEN = "\\";
	public static final String QUERY_START_TOKEN = "SELECT";
	public static final String QUERY_FROM_TOKEN = "FROM";
	public static final String SPLIT_REGEX = "[ ]+|[,][ ]*";
	public static final String ROW_REGEX = ",";
	public static final String DATABASE_LOCATION = "C:\\Users\\gursingh28\\Desktop\\Database";
	public static final String FILE_EXTENSION = ".csv";
}
