package com.sapient.week1.Day4;

import com.sapient.week1.Day2.Read;

public class Demo4 {
	public static final String ERR_MESSAGE = "Incorrect Query Format.";
	public static final String USAGE_MESSAGE = "SELECT <Column1>, <Column2>, ... FROM <TableName>";
	public static final String QUERY_MESSAGE = "Enter Query";
	
	public static void main(String[] args) {
		SqlDatabase db = new SqlDatabase();
		System.out.println(QUERY_MESSAGE);
		String query = Read.sc.nextLine();
		try {
			db.executeQuery(query);
		} catch (Exception e) {
			System.out.println(ERR_MESSAGE);
			System.out.println(USAGE_MESSAGE);
			e.printStackTrace();
		}
	}
}
