package com.sapient.week1.Day4;

import java.util.*;

public class EmployeeQualificationsDemo {
	public static void main(String[] args) {
		Map<Integer, Set<String>> ob = new LinkedHashMap<>();
		ob.put(101, new LinkedHashSet<String>());
		ob.get(101).add("B.Tech.");
		ob.get(101).add("B.E.");
		ob.get(101).add("B.E.");
		System.out.println(ob.get(101));
	}
}
