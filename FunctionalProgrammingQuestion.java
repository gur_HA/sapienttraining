public class FunctionalProgrammingQuestion{
	
	// Operations permiited by the 
	public static final String ADD = "+";
	public static final String SUBTRACT = "-";
	public static final String MULTIPLY = "*";
	public static final String DIVIDE = "/";
	
	public static void main(String args[]){
		MathOperation ob[] = new MathOperation[4];
		ob[0] = (int a, int b) -> a + b;
		ob[1] = (int a, int b) -> a - b;
		ob[2] = (int a, int b) -> a * b;
		ob[3] = (int a, int b) -> a / b;
		
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		int b = in.nextInt();
		int op = in.nextInt();
		System.out.println(ob[op-1].operate(a, b));
	}

	interface MathOperation{
		int operate(int a, int b);
	}
}