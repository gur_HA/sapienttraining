package com.sapient.week2.Day3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminDAO {
	public static final String QUERY_UPDATE_STUDENT_DETAILS = "UPDATE students SET firstName, lastName, marks WHERE registrationId = ?";
	public static final String QUERY_DELETE_STUDENT_RECORDS = "DELETE FROM students WHERE registrationId = ?";
	public static final String commitCommand = "COMMIT";
	public static final int USERNAME = 1;
	public static final int PASSWORD = 2;
	public static final int ROLE = 3;
	private static AdminDAO sdao;
	private AdminDAO() {
		super();
	}
	public static AdminDAO getInstance() {
		if(sdao==null) {
			sdao = new AdminDAO();
		}
		return sdao;
	}
	public UserBean getUserDetails(String username) throws SQLException, RecordNotFoundException, ClassNotFoundException{
		Connection conn = DBConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(QUERY_UPDATE_STUDENT_DETAILS);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		List<UserBean> players = convertResultSetToList(rs);
		if(players.size()>0) {
			return players.get(0);
		}
		throw new RecordNotFoundException();
	}
	private List<UserBean> convertResultSetToList(ResultSet rs) throws SQLException {
		List<UserBean> players = new ArrayList<>();	
		while(rs.next()) {
			players.add(new UserBean(
					 rs.getString(USERNAME)
					,rs.getString(PASSWORD)
					,rs.getString(ROLE)
					));
		}
		return players;
	}
}
