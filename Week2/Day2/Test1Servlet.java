package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import com.sapient.week2.Test1Servlet.MathOp;

/**
 * Servlet implementation class Test1Servlet
 */
public class Test1Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Map<String,MathOp> operationsMap;
    
    public static final MathOp ADD = (a,b)->(a+b);
    public static final MathOp SUBTRACT = (a,b)->(a+b);
    public static final MathOp MULTIPLICATION = (a,b)->(a*b);
    public static final MathOp DIVISION = (a,b)->(a/b);
    
    public static final String ADD_STRING = "Add";
    public static final String SUBTRACT_STRING = "Subtract";
    public static final String MULTIPLICATION_STRING = "Multiplication";
    public static final String DIVISION_STRING = "Division";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test1Servlet() {
        super();
        operationsMap = new HashMap<String, MathOp>();
        operationsMap.put(ADD_STRING, ADD);
        operationsMap.put(SUBTRACT_STRING, SUBTRACT);
        operationsMap.put(MULTIPLICATION_STRING, MULTIPLICATION);
        operationsMap.put(DIVISION_STRING, DIVISION);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try{
			out.print("<html><body><center>");
			out.print("<form action='Test1Servlet' method='post'>");
			out.print("<br> Enter Number 1 <input type='text' name='a1' value='0'/>");
			out.print("<br> Enter Number 2 <input type='text' name='a2' value='0'/>");
			// Create radio buttons, get the operation that has been selected and perform that operation.
			out.print("<br><center><input type='submit' value='Submit'/></center>");
			out.print("</form></center></body></html>");
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try{
			int n1 = Integer.parseInt(request.getParameter("a1"));
			int n2 = Integer.parseInt(request.getParameter("a2"));
			int n3 = n1+n2;
			out.print("<html><body><center>");
			out.print("<form action='Test1Servlet' method='post'>");
			out.print("<br> Enter Number 1 <input type='text' name='a1' value='"+n1+"'/>");
			out.print("<br> Enter Number 2 <input type='text' name='a2' value='"+n2+"'/>");
			out.print("<br> Result : "+n3);
			out.print("<br><center><input type='submit' value='Submit'/></center>");
			out.print("</form></center></body></html>");
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	@FunctionalInterface
	interface MathOp{
		public int compute(int a, int b);
	}
}