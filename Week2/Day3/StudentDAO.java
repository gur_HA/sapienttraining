package com.sapient.week2.Day3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {
	public static final String query1 = "SELECT registration_no, firstname, lastname, marks FROM student WHERE registration_no=?";
	public static final String commitCommand = "COMMIT";
	public static final int REGISTRATION_ID = 1;
	public static final int FIRST_NAME = 2;
	public static final int LAST_NAME = 3;
	public static final int MARKS = 4;
	private static StudentDAO sdao;
	private StudentDAO() {
		super();
	}
	public static StudentDAO getInstance() {
		if(sdao==null) {
			sdao = new StudentDAO();
		}
		return sdao;
	}
	public StudentBean getStudentMarks(int registrationNo) throws SQLException, RecordNotFoundException, ClassNotFoundException{
		Connection conn = DBConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(query1);
		ps.setInt(1, registrationNo);
		ResultSet rs = ps.executeQuery();
		List<StudentBean> players = convertResultSetToList(rs);
		if(players.size()>0) {
			return players.get(0);
		}
		throw new RecordNotFoundException();
	}
	private List<StudentBean> convertResultSetToList(ResultSet rs) throws SQLException {
		List<StudentBean> players = new ArrayList<>();	
		while(rs.next()) {
			players.add(new StudentBean(
					 rs.getInt(MARKS)
					,rs.getString(FIRST_NAME)
					,rs.getString(LAST_NAME)
					,rs.getString(REGISTRATION_ID)
					));
		}
		return players;
	}
	
}
