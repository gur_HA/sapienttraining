package com.sapient.week2.Day3;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final String SESSION_MAP_ATTR = "sessionMap";
	private static final String MESSAGE_DIGEST_TECHNIQUE = "MD5";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_USERNAME = "username";
	private static final String STUDENT_SERVLET = "StudentServlet";
	private static final String ADMIN_SERVLET = "AdminServlet";
	private static final String SERVER_ERROR_SERVLET = "ServerErrorServlet";
	private static final String STUDENT_ROLE = "student";
	private static final String ADMIN_ROLE = "admin";
	private static final LoginDAO ldao = LoginDAO.getInstance();
	private static final String FORM_HTML = "<form action='LoginServlet' method='post'>"
    		+ "<br><input type='text' name='username' required placeholder='Username' autofocus=True/>"
    		+ "<br><input type='password' name='password' required placeholder='Password'/>"
    		+ "<br><center><input type='submit' value='Log In'/></center>"
    		+ "</form>";
	private static final String LOGIN_PAGE_BEGIN = "<html>"
    		+ "<body>"
    		+ "<center>"
    		+ FORM_HTML;
	private static final String LOGIN_PAGE_END = "</center>"
    		+ "</body>"
    		+ "</html>";
    private static final String LOGIN_PAGE_HTML = LOGIN_PAGE_BEGIN
    		+ LOGIN_PAGE_END;
    private static final String LOGIN_ERROR = "<br>Incorrect Username/Password";
    		
    private static final long serialVersionUID = 1L;
    
    static ServletContext context;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	try {
    		context = config.getServletContext();
    		Map<String, HttpSession> sessionMap = new TreeMap<>();
    		context.setAttribute(SESSION_MAP_ATTR, sessionMap);
    	}catch (Exception e) {
			// TODO: handle exception
		}
    	super.init(config);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			out.print(LOGIN_PAGE_HTML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getStringHashMD5(String str) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance(MESSAGE_DIGEST_TECHNIQUE);
	    m.update(str.getBytes(), 0, str.length());
	    return new BigInteger(1, m.digest()).toString(16);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			String username = request.getParameter(PARAM_USERNAME);
			String password = request.getParameter(PARAM_PASSWORD);
			UserBean bean = ldao.getUserDetails(username);
			
		    String hashedPassword = getStringHashMD5(password);
		    boolean passwordMatch = hashedPassword.hashCode()==bean.getPassword().hashCode();
		    if(passwordMatch) {
		    	Map<String, HttpSession> sessionMap = (Map<String, HttpSession>) context.getAttribute(SESSION_MAP_ATTR);
		    	sessionMap.put(username, request.getSession());
		    	String nextServlet = 	bean.getRole().equalsIgnoreCase(ADMIN_ROLE)		? ADMIN_SERVLET 	:
		    							bean.getRole().equalsIgnoreCase(STUDENT_ROLE) 	? STUDENT_SERVLET	:
		    							SERVER_ERROR_SERVLET;
		    	response.sendRedirect(nextServlet);
		    } else {
		    	// Since Password hashes don't match, we display the page again 
		    	// and add the error message to indicate the mismatch.
		    	String pageHTML = LOGIN_PAGE_BEGIN
		    			+LOGIN_ERROR
		    			+LOGIN_PAGE_END;
		    	out.print(pageHTML);
		    }
		} catch(SQLException e) {
			e.printStackTrace();
			response.sendRedirect(SERVER_ERROR_SERVLET);
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			response.sendRedirect(SERVER_ERROR_SERVLET);
		}catch(RecordNotFoundException e) {
			e.printStackTrace();
			out.print(LOGIN_PAGE_BEGIN
	    			+LOGIN_ERROR
	    			+LOGIN_PAGE_END);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect(SERVER_ERROR_SERVLET);
		}
	}
}
