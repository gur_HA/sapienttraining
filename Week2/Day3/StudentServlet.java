package com.sapient.week2.Day3;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentServlet
 */
public class StudentServlet extends HttpServlet {
	private static final String NOT_FOUND = "<br>Not found ";
	private static final StudentDAO sdao = StudentDAO.getInstance();
	private static final long serialVersionUID = 1L;
	
	//String which contains the description of the form of the Student page,
	//which carries 
	private static final String FORM_HTML = "<form action='StudentServlet' method='post'>"
    		+ "<br><input type='text' name='registrationNo' required placeholder='Enter Registration Number' autofocus=True/>"
    		+ "<br><center><input type='submit' value='Submit'/></center>"
    		+ "</form>";
    private static final String STUDENT_FORM_HTML_BEGIN="<html>"
    		+ "<body>"
    		+ CommonHTML.LOGOUT_BUTTON_HTML
    		+ "<center>"
    		+ FORM_HTML;
    private static final String STUDENT_FORM_HTML_END="</center>"
    		+ "</body>"
    		+ "</html>"; 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			out.print(STUDENT_FORM_HTML_BEGIN);
			out.print(STUDENT_FORM_HTML_END);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			out.print(STUDENT_FORM_HTML_BEGIN);
			int registrationNo = Integer.parseInt(request.getParameter("registrationNo"));
			try{
				StudentBean bean = sdao.getStudentMarks(registrationNo);
				out.print(CommonHTML.getStudentBeanHTML(bean));				
			}catch(SQLException e) {
				out.print("500 Server Error");
			}catch(ClassNotFoundException e) {
				out.print("500 Server Error");
			}catch(RecordNotFoundException e) {
				out.print(NOT_FOUND+registrationNo);
			}
			out.print(STUDENT_FORM_HTML_END);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
