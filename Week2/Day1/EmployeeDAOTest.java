package com.mavenproj.demo;

//import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


public class EmployeeDAOTest {
	
	private List<EmployeeBean> list;
	private EmployeeDAO mockedDAO;
	@Before
	public void setUp() {
		list = new ArrayList<>();
		list.add(new EmployeeBean(1,"Gurpal",10000));
		list.add(new EmployeeBean(2,"Harpreet",20000));
		list.add(new EmployeeBean(3,"Kunal",10000));
		list.add(new EmployeeBean(4,"Sarthak",10000));
		list.add(new EmployeeBean(5,"Gurpreet",30000));
		list.add(new EmployeeBean(6,"Ajit",10000));
		list.add(new EmployeeBean(7,"Pawan",20000));
		list.add(new EmployeeBean(8,"Harshpreet",10000));
		list.add(new EmployeeBean(9,"Akash",10000));
		list.add(new EmployeeBean(10,"Saurav",10000));
		list.add(new EmployeeBean(11,"Satyender",20000));
		list.add(new EmployeeBean(12,"Chinmay",20000));
		mockedDAO = Mockito.mock(EmployeeDAO.class);
		Mockito.when(mockedDAO.readData()).thenReturn(list);
		
	}
	@Test
	public void test1() {
		Assert.assertArrayEquals(list.toArray(), mockedDAO.readData().toArray());
	}

}
