package com.sapient.week2.Day1;

public class EmployeeBean {
	private int id;			// Stores the employee ID.
	private String name;	// Stores the employee name.
	private float salary;	// Stores the employee salary.
	
	// Default Constructor.
	public EmployeeBean() {	
		super();
	}
	
	// Fields Constructor.
	public EmployeeBean(int id, String name, float salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	
	// Copy Constructor.
	public EmployeeBean(EmployeeBean eb) {
		super();
		this.id = eb.getId();
		this.name = eb.getName();
		this.salary = eb.getSalary();
	}
	
	// toString method for the class.
	@Override
	public String toString() {
		return "EmployeeBean [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	// Getters and setters.
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(salary);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeBean other = (EmployeeBean) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(salary) != Float.floatToIntBits(other.salary))
			return false;
		return true;
	}
	
}
