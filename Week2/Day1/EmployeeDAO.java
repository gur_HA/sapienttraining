package com.sapient.week2.Day1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeDAO {
	public List<EmployeeBean> readData() {
		List<EmployeeBean> employeeList = new ArrayList<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			String line;
			line = reader.readLine();
			Map<String, Integer> columnMap = new LinkedHashMap<>();
			if(line!=null) {
				String[] columns = line.split(",");
				for (int i = 0; i < columns.length; i++) {
					columnMap.put(columns[i], i);
				}
			} else {
				reader.close();
				return null;
			}
			while((line = reader.readLine())!=null) {
				String[] data = line.split(",");
				int id = Integer.parseInt(data[columnMap.get(ID)]);
				String name = data[columnMap.get(NAME)];
				Float salary = Float.parseFloat(data[columnMap.get(SALARY)]);
				employeeList.add(new EmployeeBean(id, name, salary));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.err.println("File is missing check the file location.");
			return null;
		} catch (IOException ioe) {
			System.err.println("Something happened while reading the file.");
			return null;
		}
		return employeeList;
	}
	public float getTotSal(List<EmployeeBean> employeeList) {
		float totalSalary = employeeList.stream()
				.map((employee)->(employee.getSalary()))
				.reduce(0.0f, Float::sum);
		return totalSalary;
	}
	
	// takes the salary and returns the number of people earning greater than or equal to the given salary.
	public long getCount(List<EmployeeBean> employeeList, int salary) {
		return employeeList.stream()
				.filter((employee)->(employee.getSalary() >= salary ? true : false))
				.count();
	}
	
	public EmployeeBean getEmployee(List<EmployeeBean> employeeList, int id) throws UserNotFoundException {
		List<EmployeeBean> foundElements = employeeList.stream().filter((employee)->(employee.getId()==id?true:false)).collect(Collectors.toList());
		if(foundElements.size()<=0) {
			throw new UserNotFoundException("Employee id="+id+" not found.");
		}
		return foundElements.get(0);
	}
	public static final String filePath = "C:\\Users\\gursingh28\\Desktop\\Database\\Employees.csv";
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String SALARY = "salary";
}
