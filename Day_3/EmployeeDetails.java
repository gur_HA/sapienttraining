package com.sapient.week1.Day2;

public class EmployeeDetails {
	String employeeName;
	int employeeAge;
	
	EmployeeDetails(String employeeName, int employeeAge){
		this.employeeName = employeeName;
		this.employeeAge = employeeAge;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public int getEmployeeAge() {
		return employeeAge;
	}
	public void setEmployeeAge(int age) {
		this.employeeAge = age;
	}
}
