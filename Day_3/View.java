package com.sapient.week1.Day2;

import com.sapient.week1.Day1.Read;

public class View {
	public void readData(AddBean addBean) {
		System.out.println("Enter 2 Numbers: ");
		addBean.setNum1(Read.sc.nextInt());
		addBean.setNum2(Read.sc.nextInt());
	}
	public void displayData(AddBean addBean) {
		System.out.println(addBean.getNum3());
	}
}
