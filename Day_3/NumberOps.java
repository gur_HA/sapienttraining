package com.sapient.week1.Day2;

import java.util.*;
import java.util.stream.Collectors;

public class NumberOps {
	
	public static List<? extends Number> removeDuplicate(List<? extends Number> list){
		return list.stream()
				.distinct()
				.collect(Collectors.toList());
	}
	public static List<? extends Number> removeDuplicateA(List<? extends Number> list){
		Set<Number> set = new LinkedHashSet<>();
		for (Number number : list) {
			set.add(number);
		}
		List<Number> newList = new ArrayList<>(set);
		return newList;
	}
}
