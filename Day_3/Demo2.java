package com.sapient.week1.Day2;

import java.util.*;

public class Demo2 {
	public static void main(String args[]) {
		exec1();
//		exec2();
	}
	
	public static void exec1() {
		AddBean addBean = new AddBean();
		View view = new View();
		AddDAO addDAO = new AddDAO();
		view.readData(addBean);
		addDAO.compute(addBean);
		view.displayData(addBean);
		addBean = null;
		view = null;
		addDAO = null;
		System.gc();
	}
	
	public static void exec2() {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(3);
		list.add(4);
		list.add(4);	
		displayList(NumberOps.removeDuplicate(list));
	}

	public static void displayList(List<? extends Number> list) {
		int i=0;
		System.out.print("[ ");
		for (i = 0; i < list.size()-1; i++) {
			System.out.print(list.get(i)+ ", ");
		}
		System.out.print(list.get(i)+" ]");
	}
	
}
