package com.sapient.week1.Day2;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDatabase {
	private Map<Integer, EmployeeDetails> database;
	private List<Employee> employeeList;
	private Comparator<Employee> personComparator = new Comparator<Employee>() {

		@Override
		public int compare(Employee o1, Employee o2) {
			return o1.getEmployeeDetails().getEmployeeName().compareTo(o2.getEmployeeDetails().getEmployeeName());
		}
	};
	private Comparator<Employee> ageComparator = new Comparator<Employee>() {

		@Override
		public int compare(Employee o1, Employee o2) {
			int o1Age = o1.getEmployeeDetails().getEmployeeAge();
			int o2Age = o2.getEmployeeDetails().getEmployeeAge();
			if(o1Age > o2Age)
				return 1;
			else if (o1Age < o2Age)
				return -1;
			return 0;
		}
	};
	
	EmployeeDatabase(List<Employee> employeeList){
		this.database = new LinkedHashMap<>();
		for (Employee employee : employeeList) {
			database.put(employee.getEmployeeId(), employee.getEmployeeDetails());
		}
		this.employeeList = employeeList;
	}
	
	public EmployeeDetails getEmployeeWithId(int employeeId) {
		EmployeeDetails employeeDetails = this.database.get(employeeId);
		return employeeDetails;
	}
	
	public void showDataSortedByPerson() {
		List<Employee> sortedList = sortData(employeeList,personComparator);
		displayData(sortedList);
	}
	
	public void showDataSortedByAge() {
		List<Employee> sortedList = sortData(employeeList,ageComparator);
		displayData(sortedList);
	}
	
	private List<Employee> sortData(List<Employee> employeeList, Comparator<Employee> comparator) {
		return employeeList.stream()
				.sorted(comparator)
				.collect(Collectors.toList());
	}
	private void displayData(List<Employee> list) {
		for (Employee employee : list) {
			System.out.println(employee);
		}
	}
}
