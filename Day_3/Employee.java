package com.sapient.week1.Day2;

public class Employee {
	int employeeId;
	EmployeeDetails employeeDetails;
	public Employee(int employeeId, String employeeName, int employeeAge) {
		this.employeeId = employeeId;
		this.employeeDetails = new EmployeeDetails(employeeName, employeeAge);
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public String toString() {
		return ""+employeeId+", "+employeeDetails.getEmployeeName()+", "+employeeDetails.getEmployeeAge();
	}
}
