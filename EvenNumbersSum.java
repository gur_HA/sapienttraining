public class EvenNumbersSum{

	public static final int ERROR_CODE = 1;

	// Interface to create the boolean expression for even number check.
	interface BoolExp{
		public boolean evaluate(int a);
	}

	// interface to create the lambda expression for summing the numbers.
	interface ArithmeticOp{
		public int evaluate(int a, int b);
	}

	// Help function to tell about the right usage of the 
	public static void help(){
		System.out.println("Usage: java EvenNumbersSum <Integer1> <Integer2> ...(and more integers as you wish).");
	}

	// Main function.
	public static void main(String args[]){
		
		// Lambda expression assigning false if the remainder of the number modulo 2 is 1.
		// Gives True if remainder is 0.
		BoolExp evenNumber = (a) -> (a % 2 > 0) ? false : true;
		
		// Lambda Expression for sum.
		ArithmeticOp sumExp = (a, b) -> (a + b);
		
		// Initialized sum value to 0.
		int sum = 0;

		// try statement to handle any kind of exceptions
		// and abort with the usage message displayed.
		try{

			// Check if there are inputs given or not.
			// if not, throw exception.
			if(args.length>0){
				for(int i=0; i<args.length; i++){

					// Parse the arguments.
					int num = Integer.parseInt(args[i]);

					// Check for even number.
					if(evenNumber.evaluate(num)){

						// if even, add to sum.
						sum = sumExp.evaluate(num,sum);
					}
				}
			}else{

				// Throw Exception for missing data.
				throw new Exception();
			}
		}catch(Exception e){
			
			// print usage message and exit with error code - 1.
			help();
			System.exit(ERROR_CODE);
		}

		// Successfully processed all the inputs and result is ready.
		System.out.println("Sum of all even Numbers = "+sum);
	}
}