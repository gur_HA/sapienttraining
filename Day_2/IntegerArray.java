package com.sapient.week1;

import java.util.Arrays;

public class IntegerArray {
	private static final int DEFAULT_SIZE = 10;
	private static final int MIN_INDEX = 0;
	
	private int array[];
	
	IntegerArray(){
		this.array = new int [DEFAULT_SIZE];
	}
	
	IntegerArray(int array[]){
		this.array = array;
	}

	IntegerArray(IntegerArray intarr){
		this.array = intarr.getArray();
	}
	
	public void displayAll() {
		System.out.print("[");
		int i;
		for (i = 0; i < this.array.length-1; i++) {
			System.out.print(" "+this.array[i]+",");
		}
		System.out.println(" "+this.array[i]+" ]");
	}
	
	public double average() {
		double array_sum=0;
		for (int i = 0; i < this.array.length; i++) {
			array_sum += array[i];
		}  
		double array_size = this.array.length;
		return array_sum/array_size;
	}
	
	public void sort() {
		Arrays.sort(this.array);
	}

	public boolean search(int key) {
		sort();
		return Arrays.binarySearch(this.array, key) >= MIN_INDEX ? true: false ;
	}
	
	public int[] getArray() {
		return array;
	}

	public void setArray(int[] array) {
		this.array = array;
	}

}
