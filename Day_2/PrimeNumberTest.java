package com.sapient.week1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PrimeNumberTest {

	@Test
	void test() {
		Assert.assertEquals(true, PrimeNumber.check(2));
		Assert.assertEquals(true, PrimeNumber.check(3));
		Assert.assertEquals(false, PrimeNumber.check(4));
		Assert.assertEquals(true, PrimeNumber.check(5));
		Assert.assertEquals(false, PrimeNumber.check(6));
		Assert.assertEquals(true, PrimeNumber.check(7));
		Assert.assertEquals(false, PrimeNumber.check(8));
		Assert.assertEquals(false, PrimeNumber.check(9));
	}

}
