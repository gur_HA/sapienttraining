package com.sapient.week1;

public class PrimeNumber{
	
	// Function to check if a number is prime or not.
	// Returns a boolean to indicate the decision.
	// 'true' for prime and 'false' for non-prime.
	public static boolean check(long number) {
		// Calculate the square root of the number provided.
		double squareRoot = Math.sqrt(number);
		
		// Keep on dividing the number starting from 2.
		for(long divider=2; divider <= squareRoot; divider++) {
			
			// If the number turns out to be divisible by the current divider,
			// then its not a prime.
			if( number % divider == 0 ) {
				return false;				
			}
		}
		
		// Since no divider could fully divide it, it is a prime number.
		return true;
	}
}
