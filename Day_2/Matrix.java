package com.sapient.week1;

public class Matrix {
	private int mat[][];
	public static final int DEFAULT_SIZE = 3;
	
	Matrix(){
		mat = new int [DEFAULT_SIZE][DEFAULT_SIZE];
	}
	
	Matrix(Matrix matrix) {
		this.mat = matrix.getMat();
	}
	
	Matrix(int mat[][]){
		this.mat = mat;
	}
	
	public int[][] getMat() {
		return mat;
	}

	public void setMat(int[][] mat) {
		this.mat = mat;
	}
	
	public Matrix add(Matrix matrix) throws Exception {
		int mat2[][] = matrix.getMat();
		if(!checkMatrixOrderMatch(mat2)) {
			throw new Exception("Unequal sizes.");
		}
		int rows = mat.length;
		int columns = mat[0].length;
		int result_mat[][] = new int[rows][columns];
		for(int row=0;row<rows;row++) {
			for(int column=0; column<columns; column++) {
				result_mat[row][column] = this.mat[row][column] + mat[row][column];
			}
		}
		return new Matrix(result_mat);
	}
	
	public Matrix subtract(Matrix matrix) throws Exception {
		int mat2[][] = matrix.getMat();
		if(!checkMatrixOrderMatch(mat2)) {
			throw new Exception("Unequal sizes.");
		}
		int rows = mat.length;
		int columns = mat[0].length;
		int result_mat[][] = new int[rows][columns];
		for(int row=0;row<rows;row++) {
			for(int column=0; column<columns; column++) {
				result_mat[row][column] = this.mat[row][column] + mat[row][column];
			}
		}
		return new Matrix(result_mat);
	}
	
	public void matProd(Matrix matrix) throws Exception{
		// product of matrix.
	}

	private boolean checkMatrixOrderMatch(int mat[][]) {
		int matRows = this.mat.length;
		int matColumns = this.mat[0].length; 
		int matrixRows = mat.length;
		int matrixColumns = mat.length;
		if(matRows!=matrixRows || matColumns!=matrixColumns) {
			return false;
		}
		return true;
	}
	
	private boolean checkMatrixProductOrderMatch(int mat[][]) {
		int matColumns = this.mat[0].length; 
		int matrixRows = mat.length;
		if(matColumns!=matrixRows) {
			return false;
		}
		return true;
	}
}
